# code-generation-jpa

#### 介绍
jpa db代码生成 domain、repository、service

#### 软件架构
软件架构说明



#### 使用说明

1.  修改 com.ken.work.tools.tools.Contants 中的base_package、base_package、base_out 以及 数据库信息
2.  运行：com.ken.work.tools.tools.Test 生成全部表
3.  com.slyak.spring.jpa.GenericJpaRepository  带有模板动态查询的jpa方案（当然spring-data-jpa-extra有不完善的地方~_~），
https://github.com/slyak/spring-data-jpa-extra
如果不喜欢可以换成 JpaRepository

后面有空方案换成 https://blinkfox.github.io/fenix/#/quick-install 把

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
