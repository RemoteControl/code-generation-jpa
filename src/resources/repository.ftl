package ${packageName!''};

import org.springframework.data.jpa.repository.JpaRepository;
import com.slyak.spring.jpa.GenericJpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.transaction.annotation.Transactional;

import ${modelClass!''};
/**
 * @desc {${modelDesc!''} }
 * @author cg tool by zhao
 * @date ${.now}
 * @version v1.0
 */
@Transactional
public interface ${className!''} extends GenericJpaRepository<${modelName!''}, Long> {

}
