package ${packageName!''};

import java.util.List;

import org.springframework.data.domain.Sort;
import ${modelClass!''};

/**
 * @desc {${modelDesc!''} 服务}
 * @author cg tool by zhao
 * @date ${.now}
 * @version v1.0-
 */
public interface ${className!''} {

    /**
     * 按顺序查询所有信息
     * @param sort
     * @return
     */
    public List<${modelName!''}> findAll(Sort sort);

    /**
     * 添加数据
     * @param entities
     * @return
     */
    public ${modelName!''} save(${modelName!''} entitie);


    /**
     * 添加或修改数据
     * @param entity
     * @return
     */
    public ${modelName!''} saveAndFlush(${modelName!''} entity);

    
    /**
     * 根据主键删除数据
     * @param id
     */
    public void delete(Long id);


    /**
     * 根据主键查询信息
     * @param id
     * @return
     */
    public ${modelName!''} getOne(Long id);

    
}
