package ${packageName};

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import ${daoClass};
import ${modelClass};
import ${serviceClass};



/**
 * @desc {${modelDesc!''} 服务}
 * @author cg tool by zhao
 * @date ${.now}
 * @version v1.0-
 */
@Slf4j
@Service
@CacheConfig(cacheNames = "${modelName!''}")
public class ${className} implements ${interfaceClass}{
    
    @Autowired
    private ${daoName} ${daoMethodName};
    
    @Override
    public List<${modelName}> findAll(Sort sort) {
        // TODO Auto-generated method stub
        return ${daoMethodName}.findAll(sort);
    }

    @Override
    public ${modelName} save(${modelName} entity) {
        // TODO Auto-generated method stub
        return ${daoMethodName}.save(entity);
    }

    @Override
    public ${modelName} saveAndFlush(${modelName} entity) {
        // TODO Auto-generated method stub
        return ${daoMethodName}.saveAndFlush(entity);
    }

    @Override
    public void delete(Long id) {
        // TODO Auto-generated method stub
        ${daoMethodName}.findById(id);
    }

    @Override
    public ${modelName} getOne(Long id) {
        // TODO Auto-generated method stub
        return ${daoMethodName}.getOne(id);
    }
}
