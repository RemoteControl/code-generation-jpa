package ${packageName};

import java.io.Serializable;
import com.edu.intekids.suport.BaseDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
<#if packageList??>
<#list packageList as packageData>
${packageData}
</#list>
</#if>

/**
 * @desc {${tableDesc}}
 * @author cg tool by zhao
 * @date ${.now}
 * @version v1.0
 */

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "${tableName}")
@org.hibernate.annotations.Table(appliesTo = "${tableName}", comment = "${tableDesc}")
@ApiModel(value="${tableDesc}")
public class ${className} extends BaseDomain {

    private static final long serialVersionUID = 1L;

<#list columnList as info>
    <#if (info.remarks)??>
    <#if info.remarks !=''>
     //字段说明 ${info.remarks!''}
    </#if>
    </#if>
    @Column(columnDefinition = "${info.dbColumnType!''}${info.dbColumnLengthStr!''} COMMENT '${info.remarks!''}'")
    @ApiModelProperty(value = "${info.remarks!''}")
    private ${info.columnType!''} ${info.columnName!''};
    
</#list>
    

}
