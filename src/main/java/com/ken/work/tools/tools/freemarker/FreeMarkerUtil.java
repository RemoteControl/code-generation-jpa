package com.ken.work.tools.tools.freemarker;

import java.io.File;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class FreeMarkerUtil {
	
	/**
	 * 加载 freemarker模板
	 * @param ftlPath
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	public static Template getTemplate(String ftlPath) throws Exception{
		 //1.创建配置实例Cofiguration  
        Configuration cfg = new Configuration();  
        cfg.setDirectoryForTemplateLoading(new File("src/resources"));  
        Template template = cfg.getTemplate(ftlPath);
        return template;
	}
}
