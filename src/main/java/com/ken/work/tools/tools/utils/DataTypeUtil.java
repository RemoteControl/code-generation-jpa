/**
 * 
 */
package com.ken.work.tools.tools.utils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.ken.work.tools.tools.model.ColumnModel;

/**
 * @desc {类的描述}
 * @author Liuk
 * @date 2017年4月20日 下午6:45:46
 * @version v1.0-
 */
public class DataTypeUtil {
	
	public static Set<String> getDataPackage(List<ColumnModel> list){
		Set<String> set = new HashSet<>();
		for (ColumnModel model : list) {
			if("BigDecimal".equals(model.getColumnType())){
				set.add("import java.math.BigDecimal;");
			}
			if("Date".equals(model.getColumnType())){
				set.add("import java.util.Date;");
			}
		}
		return set;
	}

	public static String getJavaType(String sqlType) {
		String result = null;
		switch (sqlType) {
		case "VARCHAR":
			result = "String";
			break;
		case "CHAR":
			result = "String";
			break;
		case "BLOB":
			result = "byte[]";
			break;
		case "TEXT":
			result = "String";
			break;
		case "INTEGER":
			result = "Long";
			break;
		case "TINYINT":
			result = "Long";
			break;
		case "INT":
			result = "Integer";
			break;
		case "SMALLINT":
			result = "Integer";
			break;
		case "MEDIUMINT":
			result = "Integer";
			break;
		case "INT UNSIGNED":
			result = "Integer";
			break;
		case "PK":
			result = "Integer";
			break;
		case "BIT":
			result = "Boolean";
			break;
		case "BIGINT":
			result = "Long";
			break;
		case "FLOAT":
			result = "Float";
			break;
		case "DOUBLE":
			result = "DOUBLE";
			break;
		case "DECIMAL":
			result = "BigDecimal";
			break;
		case "DATE":
			result = "Date";
			break;
		case "DATETIME":
			result = "Date";
			
			break;
		case "TIMESTAMP":
			result = "Date";
			break;
		default:
			break;
		}
		return result;
	}
}
