/**
 * 
 */
package com.ken.work.tools.tools.generation;

import java.io.File;

import com.ken.work.tools.tools.Contants;

/**
 * @desc {代码生成的主类}
 * @author Liuk
 * @date 2017年4月21日 上午11:53:16
 * @version v1.0-
 */
public class Running {

	/**
	 * 生成数据库中所有的表
	 */
	public static void run(){
		init();
		ModelRunning.generation();
		DaoRunning.generation();
		ServiceRunning.generation();
		ServiceImplRunning.generation();
	}
	
	
	/**
	 * 生成数据库中对应的的表
	 */
	public static void run(String tableName){
		init();
		try {
			ModelRunning.generation(tableName);
			DaoRunning.generation(tableName);
			ServiceRunning.generation(tableName);
			ServiceImplRunning.generation(tableName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("---------------code generation failed-----------------");
			e.printStackTrace();
		}
		
	}
	/**
	 * 初始化，并创建生成代码的文件夹
	 */
	public static void init(){
//		File modelFile = new File(Contants.Model.outPath);
//		if(!modelFile.exists()){
//			modelFile.mkdirs();
//		}
//		File daoFile = new File(Contants.Dao.outPath);
//		if(!daoFile.exists()){
//			daoFile.mkdirs();
//		}
//		File serviceFile = new File(Contants.Service.outPath);
//		if(!serviceFile.exists()){
//			modelFile.mkdirs();
//		}
//		File serviceImplFile = new File(Contants.ServiceImpl.outPath);
//		if(!serviceImplFile.exists()){
//			serviceImplFile.mkdirs();
//		}
	}
	
	
}
