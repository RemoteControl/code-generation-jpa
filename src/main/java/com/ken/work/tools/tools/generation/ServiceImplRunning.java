/**
 * 
 */
package com.ken.work.tools.tools.generation;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

import com.ken.work.tools.tools.Contants;
import com.ken.work.tools.tools.db.DBUtil;
import com.ken.work.tools.tools.freemarker.FreeMarkerUtil;
import com.ken.work.tools.tools.model.ServiceImplModel;
import com.ken.work.tools.tools.utils.UnderlineCamel;

import cn.hutool.core.io.FileUtil;
import freemarker.template.Template;

/**
 * @desc {类的描述}
 * @author Liuk
 * @date 2017年4月21日 上午11:56:38
 * @version v1.0-
 */
public class ServiceImplRunning {
	/**
	 * 生成dao代码
	 * @param packageName package包
	 * @param filePath 导出到哪个路径
	 * @throws Exception
	 */
	public static void generation(){
		try {
			System.out.println("---------------code generation {{service impl}} running ----------------");
			//获取数据中的所有表
			List<String> tableList = DBUtil.getTableName();
			for (String tableName : tableList) {
				generation(tableName);
			}
			System.out.println("---------------code generation {{service impl}} end-----------------");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("---------------code generation {{service impl}} failed-----------------");
			e.printStackTrace();
		}
		
	}
	
	public static void generation(String tableName) throws Exception{
		ServiceImplModel model = getServiceModel(tableName);
		Template template = FreeMarkerUtil.getTemplate("serviceimpl.ftl");
		System.out.println("---------------code generation {{table}} " + tableName +" {{Class}} " + model.getClassName() + " successed-----------------");
		FileWriter fileWriter = new FileWriter(FileUtil.newFile(Contants.ServiceImpl.getOutPath(model.getModelName()) + File.separator + model.getClassName() + ".java"));
		template.process(model, fileWriter);
	}
	
	private static ServiceImplModel getServiceModel(String tableName){
		ServiceImplModel model = new ServiceImplModel();
		String className = UnderlineCamel.underline2Camel(tableName, true,Contants.table_pre);
//		model.setDaoMethodName(className + "");
		model.setDaoMethodName( "repository");
		
		className = UnderlineCamel.getFirstUpper(className);
		model.setClassName(className + "ServiceImpl");
		model.setModelName(className + "");
		model.setModelClass(Contants.Model.getPackagename(className) +"." + className + "");
		model.setPackageName(Contants.ServiceImpl.getPackagename(className));
		model.setInterfaceClass(className + "Service");
		model.setDaoClass(Contants.Dao.getPackagename(className) +"." + className + "Repository");
		model.setDaoName(className + "Repository");
		model.setServiceClass(Contants.Service.getPackagename(className) +"." + className + "Service");
		
		model.setModelDesc(DBUtil.getTableNote(tableName));
		return model;
	}
}
