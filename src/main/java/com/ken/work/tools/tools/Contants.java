/**
 * 
 */
package com.ken.work.tools.tools;

import cn.hutool.core.io.FileUtil;

/**
 * @desc {代码生成工具常量}
 * @author Liuk
 * @date 2017年4月20日 下午7:53:08
 * @version v1.0
 */
public interface Contants {
	
	public static final String table_pre = "t_";
	
	public static final String base_package = "com.edu.intekids";
	public static final String base_out = "D:/code";
	//是否需要包含 类名
	public static final boolean needObjectFolder = true;
	
	 public static String getBasePackage(String objName) {
		if(needObjectFolder)
			return base_package+"."+objName;
		else
			return base_package;
	}
	 public static String getBasePath(String objName) {
		String path = null;
		if(needObjectFolder)
			path =  base_out+"/"+objName;
		else
			path =  base_out;
		
		if(!FileUtil.exist(path)) {
			FileUtil.mkdir(path);
		}
		
		return path;
	}
	
	/**
	 * 
	 * @desc {数据库连接参数}
	 * @author Liuk
	 * @date 2017年4月20日 下午7:57:47
	 * @version v1.0
	 */
	public interface DB {
		public static final String driver = "com.mysql.cj.jdbc.Driver";
		public static final String url = "jdbc:mysql://127.0.0.1:3306/geek_inte_kids?useSSL=false&characterEncoding=UTF-8&serverTimezone=UTC";
		public static final String username = "root";
		public static final String password = "000000";

	}

	/**
	 * 
	 * @desc {model 代码生成 参数}
	 * @author Liuk
	 * @date 2017年4月20日 下午7:58:14
	 * @version v1.0
	 */
	public interface Model{
		//model package
		public static final String packagename = base_package+".domain";
		//生成文件的目录
		public static final String outPath = base_out+"/domain";
		
		public static String getPackagename(String objName) {
			return Contants.getBasePackage(objName)+".domain";
		 }
		
		 public static String getOutPath(String objName) {
			String path = Contants.getBasePath(objName)+"/domain";
			
			if(!FileUtil.exist(path)) {
				FileUtil.mkdir(path);
			}
			
			
			return path ;
		}
	}
	
	/**
	 * 
	 * @desc {dao 代码生成 参数}
	 * @author Liuk
	 * @date 2017年4月20日 下午7:58:14
	 * @version v1.0
	 */
	public interface Dao{
		//dao package
		public static final String packagename = base_package+".repository";
		//生成文件的目录
		public static final String outPath = base_out+"/repository";
		
		public static String getPackagename(String objName) {
			return Contants.getBasePackage(objName)+".repository";
		 }
		
		 public static String getOutPath(String objName) {
			 
			String path = Contants.getBasePath(objName)+"/repository";
			if(!FileUtil.exist(path)) {
				FileUtil.mkdir(path);
			}
			
			return path;
		}
	}
	
	/**
	 * 
	 * @desc {service 代码生成 参数}
	 * @author Liuk
	 * @date 2017年4月20日 下午7:58:14
	 * @version v1.0-
	 */
	public interface Service{
		//dao package
		public static final String packagename = base_package+".service";
		//生成文件的目录
		public static final String outPath = base_out+"/service";
		
		public static String getPackagename(String objName) {
			return Contants.getBasePackage(objName)+".service";
		 }
		
		 public static String getOutPath(String objName) {
			String path = Contants.getBasePath(objName)+"/service";
			if(!FileUtil.exist(path)) {
				FileUtil.mkdir(path);
			}
			return path;
		}
	}
	
	/**
	 * 
	 * @desc {serviceImpl 代码生成 参数}
	 * @author Liuk
	 * @date 2017年4月20日 下午7:58:14
	 * @version v1.0
	 */
	public interface ServiceImpl{
		//dao package
		public static final String packagename = base_package+".service.impl";
		//生成文件的目录
		public static final String outPath = base_out+"/service/impl";
		
		
		
		 public static String getPackagename(String objName) {
			return Contants.getBasePackage(objName)+".service.impl";
		 }
		
		 public static String getOutPath(String objName) {
			String path = Contants.getBasePath(objName)+"/service/impl";
			if(!FileUtil.exist(path)) {
				FileUtil.mkdir(path);
			}
			return path;
		}
	}
}
