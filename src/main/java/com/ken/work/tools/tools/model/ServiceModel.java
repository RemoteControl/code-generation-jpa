/**
 * 
 */
package com.ken.work.tools.tools.model;

/**
 * @desc {类的描述}
 * @author Liuk
 * @date 2017年4月21日 上午11:37:56
 * @version v1.0-
 */
public class ServiceModel {

	private String packageName;
	
	private String className;
	
	private String modelName;
	
	private String modelClass;
	
	private String modelDesc;

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getModelClass() {
		return modelClass;
	}

	public void setModelClass(String modelClass) {
		this.modelClass = modelClass;
	}

	/**
	 * @return the modelDesc
	 */
	public String getModelDesc() {
		return modelDesc;
	}

	/**
	 * @param modelDesc the modelDesc to set
	 */
	public void setModelDesc(String modelDesc) {
		this.modelDesc = modelDesc;
	}

	
	
	
}
