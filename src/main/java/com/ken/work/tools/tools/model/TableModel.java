/**
 * 
 */
package com.ken.work.tools.tools.model;

import java.util.List;
import java.util.Set;

/**
 * @desc {类的描述}
 * @author Liuk
 * @date 2017年4月20日 下午6:47:29
 * @version v1.0-
 */
public class TableModel {
	
	//包名
	private String packageName;
	//表名
	private String tableName;
	//类名
	private String className;
	//字段列表
	private List<ColumnModel> columnList;
	//表名
	private String tableDesc;
	
	private Set<String> packageList;

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public List<ColumnModel> getColumnList() {
		return columnList;
	}

	public void setColumnList(List<ColumnModel> columnList) {
		this.columnList = columnList;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	
	public Set<String> getPackageList() {
		return packageList;
	}

	public void setPackageList(Set<String> packageList) {
		this.packageList = packageList;
	}

	@Override
	public String toString() {
		return "TableModel [packageName=" + packageName + ", tableName=" + tableName + ", className=" + className
				+ ", columnList=" + columnList + "]";
	}

	/**
	 * @return the tableDesc
	 */
	public String getTableDesc() {
		return tableDesc;
	}

	/**
	 * @param tableDesc the tableDesc to set
	 */
	public void setTableDesc(String tableDesc) {
		this.tableDesc = tableDesc;
	}
	
	

}
