/**
 * 
 */
package com.ken.work.tools.tools.model;

/**
 * @desc {类的描述}
 * @author Liuk
 * @date 2017年4月21日 上午11:57:14
 * @version v1.0-
 */
public class ServiceImplModel {

	private String packageName;
	private String interfaceClass;
	private String className;
	private String modelName;
	private String modelClass;
	private String daoMethodName;
	private String daoClass;
	private String daoName;
	private String serviceClass;
	
	private String modelDesc;

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getInterfaceClass() {
		return interfaceClass;
	}

	public void setInterfaceClass(String interfaceClass) {
		this.interfaceClass = interfaceClass;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getModelClass() {
		return modelClass;
	}

	public void setModelClass(String modelClass) {
		this.modelClass = modelClass;
	}

	public String getDaoMethodName() {
		return daoMethodName;
	}

	public void setDaoMethodName(String daoMethodName) {
		this.daoMethodName = daoMethodName;
	}

	public String getDaoClass() {
		return daoClass;
	}

	public void setDaoClass(String daoClass) {
		this.daoClass = daoClass;
	}

	public String getDaoName() {
		return daoName;
	}

	public void setDaoName(String daoName) {
		this.daoName = daoName;
	}

	public String getServiceClass() {
		return serviceClass;
	}

	public void setServiceClass(String serviceClass) {
		this.serviceClass = serviceClass;
	}

	/**
	 * @return the modelDesc
	 */
	public String getModelDesc() {
		return modelDesc;
	}

	/**
	 * @param modelDesc the modelDesc to set
	 */
	public void setModelDesc(String modelDesc) {
		this.modelDesc = modelDesc;
	}
	
	
	

}
