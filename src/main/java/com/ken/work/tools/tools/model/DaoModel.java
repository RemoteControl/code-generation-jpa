/**
 * 
 */
package com.ken.work.tools.tools.model;

/**
 * @desc {类的描述}
 * @author Liuk
 * @date 2017年4月20日 下午7:45:21
 * @version v1.0-
 */
public class DaoModel {

	private String packageName;
	private String modelClass;
	private String modelName;
	private String className;
	
	private String modelDesc;


	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getModelClass() {
		return modelClass;
	}

	public void setModelClass(String modelClass) {
		this.modelClass = modelClass;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @return the modelDesc
	 */
	public String getModelDesc() {
		return modelDesc;
	}

	/**
	 * @param modelDesc the modelDesc to set
	 */
	public void setModelDesc(String modelDesc) {
		this.modelDesc = modelDesc;
	}

}
