/**
 * 
 */
package com.ken.work.tools.tools.model;

/**
 * @desc {类的描述}
 * @author Liuk
 * @date 2017年4月20日 下午6:05:01
 * @version v1.0-
 */
public class ColumnModel {

	//java model 中的属性名称
	private String columnName;
	//java model 中 setting getting 中的方法名
	private String methodName;
	//java model 中的属性的类型
	private String columnType;
	//字段的备注
	private String remarks;
	
	//db中字段类型
	private String dbColumnType;
	//字段长度（可能无）
	private Integer dbColumnLength;
	//特有得decimal精度
	private Integer decimalDigits;
	

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getColumnType() {
		return columnType;
	}

	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "ColumnModel [columnName=" + columnName + ", methodName=" + methodName + ", columnType=" + columnType
				+ ", remarks=" + remarks + "]";
	}

	


	/**
	 * @return the dbColumnType
	 */
	public String getDbColumnType() {
		return dbColumnType;
	}

	/**
	 * @param dbColumnType the dbColumnType to set
	 */
	public void setDbColumnType(String dbColumnType) {
		this.dbColumnType = dbColumnType;
	}

	/**
	 * @return the dbColumnLength
	 */
	public Integer getDbColumnLength() {
		return dbColumnLength;
	}

	/**
	 * @param dbColumnLength the dbColumnLength to set
	 */
	public void setDbColumnLength(Integer dbColumnLength) {
		this.dbColumnLength = dbColumnLength;
	}
	


	/**
	 * @return the decimalDigits
	 */
	public Integer getDecimalDigits() {
		return decimalDigits;
	}

	/**
	 * @param decimalDigits the decimalDigits to set
	 */
	public void setDecimalDigits(Integer decimalDigits) {
		this.decimalDigits = decimalDigits;
	}
	
	public String getDbColumnLengthStr() {
		if(this.dbColumnLength == null)
			return "";
		else {
			
			
			if(decimalDigits == null) {
				return "("+this.dbColumnLength+")";
			}else
				return "("+this.dbColumnLength+","+decimalDigits+")";
			
		}
			
	}


}
